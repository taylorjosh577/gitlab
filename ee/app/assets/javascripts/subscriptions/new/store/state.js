import { STEPS } from '../constants';

export default () => ({
  currentStep: STEPS[0],
});
